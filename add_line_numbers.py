import sys
import os


if (len(sys.argv) != 3):
    print("This program adds a line number to every line of a file.")
    print("Two arguments needed:")
    print("- the name of the input file")
    print("- the name of the output file")
    print("eg.: add_line_numbers.py noNumbers.txt withNumbers.txt")
    sys.exit(1)

filepath = sys.argv[1]
writepath = sys.argv[2]
    
if not os.path.isfile(filepath):
    print("File path {} does not exist. Exiting...".format(filepath))
    sys.exit(1)

print("Running...")

with open(filepath) as fp:
    line = fp.readline()
    cnt = 1
    while line:
        out_file = open(writepath, "a")
        out_file.write("{}: {}\n".format(cnt,line.strip()))
        line = fp.readline()  
        cnt += 1

print("All done.")

out_file.close()
