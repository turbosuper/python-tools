import sys
import os
import numpy as np
import re

#Function based on 'Min-Max feature scaling' formula from https://en.wikipedia.org/wiki/Normalization_(statistics)
def scale_range (input, min, max):
    input_max = np.max(input)	
    input_min = np.min(input)	
    input = (input-input_min)
    input = input*(max-min)
    input = input/(input_max - input_min)
    input += min
    return input


def find_float(input):
	temp_list = []
	number = re.findall(r"[-+]?\d*\.\d+|\d+", input)
	temp_list.append(number) 
	return temp_lis

def main():
	if (len(sys.argv) != 6):
	    print("This program scales all values saved in a file to a given range, and rounds them up.")
	    print("Program keeps the column structure of the input file.")
	    print("Four arguments needed:")
	    print("- the name of the input file")
	    print("- the minimum of the range")
	    print("- the maximum of the range")
	    print("- the name of the output file")
	    print("- the number of digits for rounding")
	    print("eg.: multiply_by_2_to_the_power.py unscaledValues.txt -100 100 scaledValues.txt 4")
	    sys.exit(1)

	filepath = sys.argv[1]
	range_min = float(sys.argv[2])
	range_max = float(sys.argv[3])
	writepath = sys.argv[4]
	round_digits = int(sys.argv[5])
	    
	if not os.path.isfile(filepath):
	    print("File path {} does not exist. Exiting...".format(filepath))
	    sys.exit(1)
	    
	#if not (isinstance(int(powerOfTwo),int) ):
	    #print("Argument {} is not na integer. Exiting...".format(powerOfTwo))
	    #sys.exit(1)

	print("Range is set from {} to {} now.".format(range_min, range_max))


	print("===============Starting.....================")
	#get number of lines and colums, to create a matrix
	with open(filepath) as f:
		first_line = next(f)
		col_cnt = len(first_line.split())
	f.close()

	with open(filepath) as flines:
		some_line = flines.readline()
		line_cnt = 1
		while some_line:
			line_cnt += 1
			some_line = flines.readline()
	flines.close()
	print("Found {} columns with {} lines in the file {}".format(col_cnt, line_cnt, filepath))

	values_matrix = [[0 for x in range(line_cnt-1)] for y in range(col_cnt)]
        
	#open the file and read the values into the matrix
	with open(filepath) as fp:
		cnt = 1
		i = 0
		for line in fp:
			temp_list = line.split(" ")
			#remove newline character when it is the last one
			if temp_list[-1] == '\n':
				print("Found the newline character in the line {}, removing...".format(cnt))
				temp_list.pop()
                               
			#extract values from each line
			extracted_numbers = []
			extracted_numbers = [float(n) for n in line.split()]
#			
			#generate matrix with values
			i = 0
			while i < col_cnt:
                            values_matrix[i][cnt-1] = extracted_numbers[i]
                            i+=1
			cnt+=1			      

	#scale the matrix values to a given range
	values_array_np = np.array(values_matrix)
	print("Unscaled values: {}".format(values_array_np))
	scaled_matrix = scale_range(values_array_np, range_min, range_max)
	print("Scaled values: {}". format(scaled_matrix))
	

	#round and save values to a file
	print("Rounding, and writing to a file {} ...".format(writepath))
	k = 0
	out_file = open(writepath, "a")
	while k < line_cnt-1:
		j = 0
		while j < col_cnt:
			print("value before rounding is: {}".format(scaled_matrix[j][k]))
			temp_value = round(scaled_matrix[j][k], round_digits)
			print("temp_value after rounding is: {}".format(temp_value))
			if round_digits == 0:
                            out_file.write("%d "% (temp_value))
			else:    
                            out_file.write("%f "% (temp_value))
			j+=1
		out_file.write("\n")
		j = 0
		k+=1


	out_file.close()
	print("All done.")
	
if __name__ == "__main__":
	main()
