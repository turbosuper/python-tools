import sys
import os



if (len(sys.argv) != 4):
    print("This program multiplies each number in a file with  2^x")
    print("Three arguments needed:")
    print("- the name of the input file")
    print("- the power of twos, with which every number will be multiplied with (must be an integer)")
    print("- the name of the output file")
    print("eg.: multiply_by_2_to_the_power.py initialValues.txt 8 valuesMultipliedBy2tothe8.txt")
    sys.exit(1)

filepath = sys.argv[1]
powerOfTwo = int(sys.argv[2])
writepath = sys.argv[3]
    
if not os.path.isfile(filepath):
    print("File path {} does not exist. Exiting...".format(filepath))
    sys.exit(1)
    
#if not (isinstance(int(powerOfTwo),int) ):
    #print("Argument {} is not na integer. Exiting...".format(powerOfTwo))
    #sys.exit(1)

with open(filepath) as fp:
    line = fp.readline()
    cnt = 1
    while line:
        temp_list = line.split(" ")
        #remove newline character when it is the last one
        if temp_list[-1] == '\n':
            print("Found the newline character in the line {}, removing...".format(cnt))
            temp_list.pop()

        temp_values = [float(values) for values in temp_list]
        temp_x, temp_y = temp_values
        temp_x = (temp_x * (2**powerOfTwo))
        temp_y = (temp_y * (2**powerOfTwo))

        out_file = open(writepath, "a")
        out_file.write("{} {}\n".format(temp_x,temp_y))
        line = fp.readline()  
        cnt += 1

out_file.close()
