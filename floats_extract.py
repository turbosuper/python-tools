import sys
import os
import numpy as np
import re

def find_float(input):
	temp_list = []
	print("line just now being read is: {} ".format(input))
	#Solution 1
#	for num in input.split(' '): 
#		if num.replace('-', '').strip().isdigit(): 
#			temp_list.append(int(num)) 
#		else: 
#			try: 
#				temp_list.append(float(num)) 
#			except Exception: 
#				pass 
#	print("Found list is: {}".format(temp_list))
	number = [float(n) for n in input.split()]
	print("Found number is: {}".format(number))
	temp_list = number 
	return temp_list

def main():
	if (len(sys.argv) != 3):
	    print("This program finds all numbers in a text and saves them to a file, each row saved in a separate list")
	    print("Two arguments needed:")
	    print("- the name of the input file")
	    print("- the name of the output file")
	    print("eg.: floats_extract.py rawText.txt justFloats.txt")
	    sys.exit(1)

	filepath = sys.argv[1]
	writepath = sys.argv[2]
	    
	if not os.path.isfile(filepath):
	    print("File path {} does not exist. Exiting...".format(filepath))
	    sys.exit(1)
	    
	print("******************************************************************************************************************************************")

	current_list = []
        
	#open the file and read each line
	with open(filepath) as fp:
		for line in fp:
			#end the line to the function
			current_list.append(find_float(line))
	
	#save the output to a file
	print("Writing to a file {} ...".format(writepath))
	out_file = open(writepath, "w")
	for item in current_list:
		out_file.write(str(item))

	out_file.close()

	print("All done.")
	
if __name__ == "__main__":
	main()
